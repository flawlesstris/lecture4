package sbp.exceptions.customException;

public class CustomException extends Exception
{
    public CustomException(String message) {
        super("Hi! This is my exception!" + message);
    }

    public CustomException(String message, Throwable cause) {
        super("Hi! This is my exception!" + message, cause);
    }
}
