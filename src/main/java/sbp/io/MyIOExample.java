package sbp.io;

import java.io.*;

public class MyIOExample {
    /**
     * Создать объект класса {@link java.io.File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     * - абсолютный путь
     * - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     * - размер
     * - время последнего изменения
     * Необходимо использовать класс {@link java.io.File}
     *
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) throws Exception {
        File myFile = new File(fileName);

        Writer writer = new FileWriter(fileName);

        writer.write("Hello!");

        writer.close();


        if (myFile.exists()) {
            System.out.println(fileName + " absolut path: " + myFile.getAbsolutePath());
            System.out.println(fileName + " path: " + myFile.getPath());
        }
        if (myFile.isFile()) {
            System.out.println(fileName + " length: " + myFile.length());
            System.out.println(fileName + " last modified: " + myFile.lastModified());
            return true;
        } else
            return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName) throws Exception {
        FileInputStream fileInputStream = new FileInputStream(sourceFileName);

        byte [] inputByteArray = new byte[500];
        fileInputStream.read(inputByteArray);

        fileInputStream.close();

        File myFile = new File(destinationFileName);

        FileOutputStream fileOutputStream = new FileOutputStream(destinationFileName);

        fileOutputStream.write(inputByteArray);

        fileOutputStream.close();

        return sourceFileName.equals(destinationFileName);
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName) throws Exception {
        InputStream inputStream = new FileInputStream(sourceFileName);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

        int inputFileName = bufferedInputStream.read();

        bufferedInputStream.close();
        inputStream.close();

        File myFile = new File(destinationFileName);

        FileOutputStream fileOutputStream = new FileOutputStream(destinationFileName);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);

        bufferedOutputStream.write(inputFileName);

        bufferedInputStream.close();
        fileOutputStream.close();

        return sourceFileName.equals(destinationFileName);
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName) throws Exception {
        Reader reader = new FileReader(sourceFileName);
        BufferedReader bufferedReader = new BufferedReader(reader);

        String inputFileName = bufferedReader.readLine();

        bufferedReader.close();
        reader.close();

        File myFile = new File(destinationFileName);

        Writer writer = new FileWriter(destinationFileName);

        writer.write(inputFileName);

        writer.close();

        return sourceFileName.equals(destinationFileName);
    }
}
